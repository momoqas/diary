import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import SubmitComment from './SubmitComment';
import _ from 'lodash';
import Comment from './Comment'
class NoteDetail extends Component {

    renderComments(){
        const {note} = this.props;
        return _.map(note.comments, (comment, key)=>{
            return (
                <Comment key={key} id={key}>
                    {comment.commentBody}
                </Comment>
            )
        })
    }
    render(){
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3">
                        <Link to="/">Back</Link>
                        <div>
                            <h2>{this.props.note.title}</h2>
                            <p>{this.props.note.body}</p>
                            <SubmitComment id={this.props.match.params.id}/>
                            {this.renderComments()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state, ownProps) {
    return {
        note : state.notes[ownProps.match.params.id],
        uid : state.user.uid
    }
}
export default connect(mapStateToProps)(NoteDetail);
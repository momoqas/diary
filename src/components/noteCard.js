import React from 'react';

/**
 * hadchi dernah ilaa bina nsiftoh par class
 * export default class NoteCard extends Component {
    render(){
        return(
            <div className="jumbotron">
               <div>{this.props.children}</div>
            </div>
        )
    }
}**/

/**hadchi nsiftoh gha b variable hada kaytsma statless component**/
const NoteCard = props => (
    <div className="jumbotron">
        <div>{props.children}</div>
    </div>
);
export default NoteCard;
import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../styles/loading.css';
//with withrouter we can get access to history object's property
import {withRouter} from 'react-router-dom';
import {getUser} from "../actions/userAction";
import {getNotes} from "../actions/notesActions";

class LoadingComponent extends Component {
    componentWillMount(){
        const {userLoading, notesLoading} = this.props;
        //if we have not tried to load user, load user
        if (userLoading === undefined){
            this.props.getUser();
        }
        //if we have not tried to load notes, load notes
        if (notesLoading === undefined){
            this.props.getNotes();
        }
    }
    componentWillReceiveProps(nextProps){
        //wait for user to get authenticated than load notes
        if(nextProps.notesLoading === -1 && nextProps.user !== null){
            this.props.getNotes();
        }
    }
    render(){
        const {userLoading, notesLoading, children} = this.props;
        if((!userLoading && !notesLoading) || this.props.user === null){
            return(
                <div>{children}</div>
            )
        } else
        {
            return(
                <div className="flex-center position-ref full-height">
                    <div className="title m-b-md">
                        Loading...
                    </div>
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        userLoading : state.loading.user,
        notesLoading : state.loading.notes
    }
}

export default withRouter(connect(mapStateToProps, {getUser, getNotes})(LoadingComponent))
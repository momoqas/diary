import React, { Component } from 'react';

//had lodash library fiha bazaf dial les fonction kay3awnok bach t executer chi haja f7al t9alab 3liha f tab
import _ from 'lodash';
import {connect} from 'react-redux';
import {getNotes,saveNote,deleteNote} from "../actions/notesActions";
import NoteCard from './noteCard';
import {getUser} from "../actions/userAction";
import {Link} from 'react-router-dom';


class App extends Component {
    constructor(props){
        super(props);
        //state
        this.state = {
            title : '',
            body : '',

        };
        //bind
        this.handleChange = this.handleChange.bind(this);
        this.hanldeSubmit = this.hanldeSubmit.bind(this);
        this.renderNotes = this.renderNotes.bind(this);
    }
    //lifeCycle
    /**componentDidMount(){
        this.props.getNotes();
        this.props.getUser();

    }**/
    //handle change
    handleChange(e){
        this.setState({
             [e.target.name] :  e.target.value
        });
    }
    //hndel submit
    hanldeSubmit(e) {
        e.preventDefault();
        const note = {
            title: this.state.title,
            body : this.state.body,
            uid : this.props.user.uid
        };
        this.props.saveNote(note);
        //nam7iw formulaire
        this.setState({
            title : '',
            body : ''

        })
    }
    //render Posts
    renderNotes (){
        return _.map(this.props.notes, (note, key) => {
            return <NoteCard key={key}>
                <Link to={`/${key}`}>
                    <h2>{note.title} </h2>
                </Link>
                <p>{note.body}</p>
                {note.uid === this.props.user.uid && (
                    <button className="btn btn-danger btn-xs" onClick={() => this.props.deleteNote(key)}>
                        Delete
                    </button>
                )}
                {note.uid === this.props.user.uid && (
                    <button className="btn btn-info btn-xs pull-right">
                        <Link to={`/${key}/edit`}>Update</Link>
                    </button>
                )}

            </NoteCard>
        });
    }
    render() {
        return (
            <div className="container-fluid">
        <div className="row">
            <div className="col-sm-2 text-center">
                <img src={this.props.user.photoURL}
                     height="100px"
                     className="img rounded-circle"
                     style={{padding : '20px'}}
                />
                <h4 className="userName">Welcome Back {this.props.user.displayName} </h4>
            </div>
            <div className="col-sm-8">
                <form onSubmit={this.hanldeSubmit}>
                    <div className="form-group">
                        <input onChange={this.handleChange}
                               type="text" name="title"
                               value={this.state.title}
                               className="form-control no-border"
                               placeholder="title" required/>
                    </div>
                    <div className="form-group">
                        <textarea
                            onChange={this.handleChange}
                            value={this.state.body}
                            type="text" name="body"
                            className="form-control no-border"
                            placeholder="message" required/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary col-sm-12">Add</button>
                    </div>
                </form>
                <br/>
                <br/>
                <br/>
                {this.renderNotes()}
            </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state, ownProps) {
    return {
        notes : state.notes,
        user : state.user,

    }
}

export default connect(mapStateToProps,{getNotes, saveNote,deleteNote, getUser})(App);

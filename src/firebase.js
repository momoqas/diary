import * as firebase from 'firebase';
// Initialize Firebase
var config = {
    apiKey: "AIzaSyCEAgbeqkQeaY6Pcey7cUT3DEodUnp36Ws",
    authDomain: "diary-84f0b.firebaseapp.com",
    databaseURL: "https://diary-84f0b.firebaseio.com",
    projectId: "diary-84f0b",
    storageBucket: "",
    messagingSenderId: "65169875947"
};
firebase.initializeApp(config);

export const database = firebase.database().ref('/notes');
export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();
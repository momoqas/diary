import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {updateNote} from '../actions/notesActions';
class NoteEdit extends Component {
    constructor(props){
        super(props);
        //state
        this.state = {
            title : this.props.note.title,
            body : this.props.note.body,

        };
        //bind
        this.handleChange = this.handleChange.bind(this);
        this.hanldeSubmit = this.hanldeSubmit.bind(this);

    }

    //handle change
    handleChange(e){
        this.setState({
            [e.target.name] :  e.target.value
        });
    }
    //hndel submit
    hanldeSubmit(e) {
        e.preventDefault();
        const note = {
            title: this.state.title,
            body : this.state.body,
            uid: this.props.uid

        };
        this.props.updateNote(this.props.match.params.id,note);
        //nam7iw formulaire
        this.setState({
            title : '',
            body : ''

        });
        this.props.history.push('/');
    }
    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3">
                        <form onSubmit={this.hanldeSubmit}>
                            <div className="form-group">
                                <input onChange={this.handleChange}
                                       type="text" name="title"
                                       value={this.state.title}
                                       className="form-control no-border"
                                       placeholder="title" required/>
                            </div>
                            <div className="form-group">
                        <textarea
                            onChange={this.handleChange}
                            value={this.state.body}
                            type="text" name="body"
                            className="form-control no-border"
                            placeholder="message" required/>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary col-sm-12">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )

    }
}
function mapStateToProps(state, ownProps) {
    return {
        note : state.notes[ownProps.match.params.id],
        uid : state.user.uid
    }
}
export default connect(mapStateToProps, {updateNote})(NoteEdit);